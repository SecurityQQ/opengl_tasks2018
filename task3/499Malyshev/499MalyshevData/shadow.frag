#version 330

uniform sampler2D diffuseTex;
uniform sampler2DShadow shadowTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)
in vec4 shadowTexCoord; //выходные текстурные координаты для проективное текстуры

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;
const vec4 offset = vec4(0.0, 0.0, -1e-5, 0.0); //отступ накладывается на текстурные координаты, чтобы избежать артефактов

void main()
{
	float visibility = textureProj(shadowTex, shadowTexCoord + offset * shadowTexCoord.w);


	//===============================

	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;

	vec3 color;

	{
		vec3 normal = normalize(normalCamSpace);
		vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

		if(dot(normal, viewDirection) > 0) {
        } else {
            normal = -normal;
        }

        {
			vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); //направление на источник света

			float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

			color = diffuseColor * (light.La + light.Ld * NdotL * visibility);

			if (NdotL > 0.0)
			{
				vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

				float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
				blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
				color += light.Ls * Ks * blinnTerm * visibility;
			}
		}
	}

	fragColor = vec4(color, 1.0);
}
