#include "Mesh.hpp"
#include <assimp/cimport.h>
#include <iostream>
#include <sstream>

MeshPtr makeGroundPlane(float size, float numTiles)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //front 1
    vertices.push_back(glm::vec3(-size, size, -2.0));
    vertices.push_back(glm::vec3(size, -size, -2.0));
    vertices.push_back(glm::vec3(size, size, -2.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, numTiles));

    //front 2
    vertices.push_back(glm::vec3(-size, size, -2.0));
    vertices.push_back(glm::vec3(-size, -size, -2.0));
    vertices.push_back(glm::vec3(size, -size, -2.0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(-numTiles, numTiles));
    texcoords.push_back(glm::vec2(-numTiles, -numTiles));
    texcoords.push_back(glm::vec2(numTiles, -numTiles));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


MeshPtr makeSphere(float radius, unsigned int N)
{
    unsigned int M = N / 2;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (unsigned int i = 0; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();

            //Первый треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta) * radius, sin(phi1) * sin(theta) * radius, cos(theta) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta), sin(phi1) * sin(theta), cos(theta)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(glm::vec3(cos(phi) * sin(theta) * radius, sin(phi) * sin(theta) * radius, cos(theta) * radius));
            vertices.push_back(glm::vec3(cos(phi) * sin(theta1) * radius, sin(phi) * sin(theta1) * radius, cos(theta1) * radius));
            vertices.push_back(glm::vec3(cos(phi1) * sin(theta1) * radius, sin(phi1) * sin(theta1) * radius, cos(theta1) * radius));

            normals.push_back(glm::vec3(cos(phi) * sin(theta), sin(phi) * sin(theta), cos(theta)));
            normals.push_back(glm::vec3(cos(phi) * sin(theta1), sin(phi) * sin(theta1), cos(theta1)));
            normals.push_back(glm::vec3(cos(phi1) * sin(theta1), sin(phi1) * sin(theta1), cos(theta1)));

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Sphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}

template<class T>
const T min(const T a, const T b)
{
    return (b < a) ? b : a;
}

template<class T>
const T max(const T a, const T b)
{
    return (b >= a) ? b : a;
}

inline double TorusX(const double r, const double v, const double u) {
    return (1.0 + r * cos(v)) * cos(u);
}

inline double TorusY(const double r, const double v, const double u) {
    return (1.0 + r * cos(v)) * sin(u);
}

inline double TorusZ(const double r, const double v, const double u) {
    return r * sin(v);
}

inline glm::vec3 TorusPoint(const double r, const double v, const double u) {
    return glm::vec3 {TorusX(r, v, u), TorusY(r, v, u), TorusZ(r, v, u)};
}


inline glm::vec3 TorusNormal(const double r, const double v, const double u) {
    auto x = cos(v) * cos(u);
    auto y = cos(v) * sin(u);
    auto z = sin(v);
    return glm::vec3 {x, y, z};
}


MeshPtr makeTorus(float size, int numberOfPoligons) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec2> texcoords;
    std::vector<glm::vec3> normals;

    for (int i = 0; i < numberOfPoligons; i++){
        double v1 = 2.0 * glm::pi<double>() / double(numberOfPoligons) * double(i);
        double v2 = 2.0 * glm::pi<double>() / double(numberOfPoligons) * double(i + 1);
        for (int j = 0; j < numberOfPoligons; j++){
            double u1 = 2.0 * glm::pi<double>() / numberOfPoligons * double(j) - glm::pi<double>();
            double u2 = 2.0 * glm::pi<double>() / numberOfPoligons  * double(j + 1) - glm::pi<double>();

            vertices.push_back(TorusPoint(size, v1, u1 ));
            vertices.push_back(TorusPoint(size, v1, u2 ));
            vertices.push_back(TorusPoint(size, v2, u1 ));

            vertices.push_back(TorusPoint(size, v2, u2));
            vertices.push_back(TorusPoint(size, v1, u2));
            vertices.push_back(TorusPoint(size, v2, u1));

            normals.push_back(TorusNormal(size,v1, u1));
            normals.push_back(TorusNormal(size,v1, u2));
            normals.push_back(TorusNormal(size,v2, u1));

            normals.push_back(TorusNormal(size,v2, u2));
            normals.push_back(TorusNormal(size,v1, u2));
            normals.push_back(TorusNormal(size,v2, u1));

            texcoords.push_back(glm::vec2(v1, u1));
            texcoords.push_back(glm::vec2(v1, u2));
            texcoords.push_back(glm::vec2(v2, u1));

            texcoords.push_back(glm::vec2(v2, u2));
            texcoords.push_back(glm::vec2(v1, u2));
            texcoords.push_back(glm::vec2(v2, u1));

        }
    }

    DataBufferPtr vertices_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    vertices_buf->setData(3 * sizeof(float) * vertices.size(), vertices.data());

    DataBufferPtr normals_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normals_buf->setData(3 * sizeof(float) * normals.size(), normals.data());

    DataBufferPtr tex_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    tex_buf->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, vertices_buf);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normals_buf);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, tex_buf);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}
