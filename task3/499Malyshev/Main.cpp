#include <Mesh.hpp>
#include <iostream>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"

#include <iostream>
#include <vector>

class SampleApplication : public Application
{
public:


    float _size = 0.5f;
    unsigned int _num_polygons = 100;

    LightInfo _light;
    CameraInfo _lightCamera;
    TexturePtr _texture;
    GLuint _sampler;

    GLuint _depthSamplerLinear;

    MeshPtr _torus;
    MeshPtr _marker;
    MeshPtr _ground;

    GLuint _framebufferId;
    GLuint _depthTexId;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;

    ShaderProgramPtr _marker_shader;
    ShaderProgramPtr _commonWithShadowsShader;
    ShaderProgramPtr _renderToShadowMapShader;

    float _lr = 5.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    void makeScene() override
    {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();
        _torus = makeTorus(_size, _num_polygons);


        _marker = makeSphere(0.1f, 100);


        _marker_shader = std::make_shared<ShaderProgram>("499MalyshevData/marker.vert", "499MalyshevData/marker.frag");
		_renderToShadowMapShader = std::make_shared<ShaderProgram>("499MalyshevData/toshadow.vert", "499MalyshevData/toshadow.frag");
		_commonWithShadowsShader = std::make_shared<ShaderProgram>("499MalyshevData/shadow.vert", "499MalyshevData/shadow.frag");

        _ground = makeGroundPlane(5.0f, 2.0f);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.4, 0.4, 0.4);
        _light.diffuse = glm::vec3(0.5, 0.5, 0.5);
        _light.specular = glm::vec3(0.6, 0.6, 0.6);



        _texture = loadTexture("499MalyshevData/texture.jpg");

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        GLfloat border[] = { 1.0f, 0.0f, 0.0f, 1.0f };

        glGenSamplers(1, &_depthSamplerLinear);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glSamplerParameterfv(_depthSamplerLinear, GL_TEXTURE_BORDER_COLOR, border);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glSamplerParameteri(_depthSamplerLinear, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);



        glGenFramebuffers(1, &_framebufferId);
		glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);


		glGenTextures(1, &_depthTexId);
		glBindTexture(GL_TEXTURE_2D, _depthTexId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, _fbWidth, _fbHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexId, 0);

		GLenum buffers[] = { GL_NONE };
		glDrawBuffers(1, buffers);

		if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		{
			std::cerr << "Failed to setup framebuffer\n";
			exit(1);
		}

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }

        }
        ImGui::End();
    }

    void drawGUI() override {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        Application::drawGUI();
    }

    void update()
    {
        Application::update();

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _lightCamera.viewMatrix = glm::lookAt(_light.position, glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        _lightCamera.projMatrix = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 30.f);
    }

    void draw() override
    {
        Application::draw();


        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        glViewport(0, 0, _fbWidth, _fbHeight);
        glClear(GL_DEPTH_BUFFER_BIT);

        _renderToShadowMapShader->use();
        _renderToShadowMapShader->setMat4Uniform("lightViewMatrix", _lightCamera.viewMatrix);
        _renderToShadowMapShader->setMat4Uniform("lightProjectionMatrix", _lightCamera.projMatrix);


        _renderToShadowMapShader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
        _renderToShadowMapShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_lightCamera.viewMatrix * _ground->modelMatrix()))));

        _ground->draw();

        _renderToShadowMapShader->setMat4Uniform("modelMatrix", _torus->modelMatrix());
        _renderToShadowMapShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_lightCamera.viewMatrix * _torus->modelMatrix()))));

        _torus->draw();


        glDisable(GL_CULL_FACE);


        glUseProgram(0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);


        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        _commonWithShadowsShader->use();

        _commonWithShadowsShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _commonWithShadowsShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        // _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;

        {
            glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0f));

            _commonWithShadowsShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            _commonWithShadowsShader->setVec3Uniform("light.La", _light.ambient);
            _commonWithShadowsShader->setVec3Uniform("light.Ld", _light.diffuse);
            _commonWithShadowsShader->setVec3Uniform("light.Ls", _light.specular);
        }

        {
            _commonWithShadowsShader->setMat4Uniform("lightViewMatrix", _lightCamera.viewMatrix);
            _commonWithShadowsShader->setMat4Uniform("lightProjectionMatrix", _lightCamera.projMatrix);

            glm::mat4 projScaleBiasMatrix = glm::scale(glm::translate(glm::mat4(1.0), glm::vec3(0.5, 0.5, 0.5)), glm::vec3(0.5, 0.5, 0.5));
            _commonWithShadowsShader->setMat4Uniform("lightScaleBiasMatrix", projScaleBiasMatrix);

        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _texture->bind();

        _commonWithShadowsShader->setIntUniform("diffuseTex", 0);

        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glBindSampler(1, _depthSamplerLinear);
        _commonWithShadowsShader->setIntUniform("shadowTex", 1);

        {
            _commonWithShadowsShader->setMat4Uniform("modelMatrix", _torus->modelMatrix());
            _commonWithShadowsShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _torus->modelMatrix()))));
            _torus->draw();
        }
        {
            _commonWithShadowsShader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
            _commonWithShadowsShader->setMat3Uniform("normalToCameraMatrix",
                                   glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
            _ground->draw();
        }
        {
            _marker_shader->use();
            _marker_shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _marker_shader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
