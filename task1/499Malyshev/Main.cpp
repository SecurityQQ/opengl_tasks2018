#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>
#include <vector>

class SampleApplication : public Application
{
public:
    MeshPtr _torus;

    ShaderProgramPtr _shader;

    int NumberOfVertex = 80;
    int VertexStep = 5;
    float ThorusRadius = 0.5f;

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        update_torus();

        _shader = std::make_shared<ShaderProgram>("499MalyshevData/shaderNormal.vert", "499MalyshevData/shader.frag");
    }

    void update_torus() {
        _torus = makeTorus(ThorusRadius, NumberOfVertex);
    }

    void update() override
    {
        Application::update();

        // Решил ради интереса поменять радиус тора со временем
        float radius = sin(static_cast<float>(glfwGetTime()));
        ThorusRadius = radius;

        update_torus();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
            Application::handleKey(key, scancode, action, mods);
            if (action == GLFW_PRESS) {
                if (key == GLFW_KEY_EQUAL) {
                    NumberOfVertex += VertexStep;
                    update_torus();
                }

                if (key == GLFW_KEY_MINUS) {
                    NumberOfVertex -= VertexStep;
                    update_torus();
                }
            }
        }


    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT Task 1. Amazing Torus.", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            ImGui::Text("Number of Poligons: %d", _torus->getNumberOfPoligons());
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _torus->modelMatrix());
        _torus->draw();

    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
