set(SRC_FILES
    Main.cpp
    common/Application.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
)

set(HEADER_FILES
    common/Application.hpp
    common/Camera.hpp
    common/Common.h
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

MAKE_TASK(499Malyshev 1 "${SRC_FILES}")
