#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"


#include <iostream>
#include <vector>

class SampleApplication : public Application
{
public:
    MeshPtr _torus;
    MeshPtr _cube;
    TexturePtr _cubeTex;
    TexturePtr _cubeTex2;
    ShaderProgramPtr _shader;
    ShaderProgramPtr _skyboxShader;

    // TexturePtr _texture;

    int NumberOfVertex = 80;
    int VertexStep = 5;
    float ThorusRadius = 0.5f;
    GLuint sampler;
    GLuint _sampler_tex;

    float _lr = 5.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;
    LightInfo _light;

    void makeScene() override
    {
        Application::makeScene();
        std::cout<<"make scene begin\n";
        _cameraMover = std::make_shared<FreeCameraMover>();

        _torus = makeTorus(ThorusRadius, NumberOfVertex);
        _torus->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));



        _shader = std::make_shared<ShaderProgram>("499MalyshevData/texture.vert", "499MalyshevData/texture.frag");
        _skyboxShader = std::make_shared<ShaderProgram>("499MalyshevData/skybox.vert", "499MalyshevData/skybox.frag");

        _cube = makeCube(3.f);
        // auto image = loadImage("499MalyshevData/texture.jpg");
        // _image = loadImage(path);
        // _texture = loadTexture(image);
        std::cout<<"make cube ok\n";
        _cubeTex = loadCubeTexture("499MalyshevData/images/cube");
        _cubeTex2 = loadTexture("499MalyshevData/images/earth_global.jpg");
        std::cout<<"load texture cube ok\n";

        _light.position =
          glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        glGenSamplers(1, &sampler);
        glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameteri(sampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler_tex);
        glSamplerParameteri(_sampler_tex, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler_tex, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler_tex, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler_tex, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void update_torus() {
        auto prevModelMatrix = _torus->modelMatrix();
        _torus = makeTorus(ThorusRadius, NumberOfVertex);
        _torus->setModelMatrix(prevModelMatrix);
        std::cout<<"make torus ok\n";
    }

    void update() override
    {
        Application::update();

        // Р РµС€РёР» СЂР°РґРё РёРЅС‚РµСЂРµСЃР° РїРѕРјРµРЅСЏС‚СЊ СЂР°РґРёСѓСЃ С‚РѕСЂР° СЃРѕ РІСЂРµРјРµРЅРµРј
        // float radius = sin(static_cast<float>(glfwGetTime()));
        // ThorusRadius = radius;

        // update_torus();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
            Application::handleKey(key, scancode, action, mods);
            if (action == GLFW_PRESS) {
                if (key == GLFW_KEY_EQUAL) {
                    NumberOfVertex += VertexStep;
                    update_torus();
                }

                if (key == GLFW_KEY_MINUS) {
                    NumberOfVertex -= VertexStep;
                    update_torus();
                }
            }
        }


    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT Task 2. Amazing Torus.", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);
            ImGui::Text("Number of Poligons: %d", _torus->getNumberOfPoligons());
        }
        ImGui::End();
    }

    void drawGUI() override {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        Application::drawGUI();
    }


    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _skyboxShader->use();

        glm::vec3 lightPos = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;

        glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]); //РР·РІР»РµРєР°РµРј РёР· РјР°С‚СЂРёС†С‹ РІРёРґР° РїРѕР»РѕР¶РµРЅРёРµ РІРёСЂС‚СѓР°Р»СЊРЅС‹Р№ РєР°РјРµСЂС‹ РІ РјРёСЂРѕРІРѕР№ СЃРёСЃС‚РµРјРµ РєРѕРѕСЂРґРёРЅР°С‚

        _skyboxShader->setVec3Uniform("cameraPos", cameraPos);
        _skyboxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _skyboxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Р”Р»СЏ РїСЂРµРѕР±СЂР°Р·РѕРІР°РЅРёСЏ РєРѕРѕСЂРґРёРЅР°С‚ РІ С‚РµРєСЃС‚СѓСЂРЅС‹Рµ РєРѕРѕСЂРґРёРЅР°С‚С‹ РЅСѓР¶РЅР° СЃРїРµС†РёР°Р»СЊРЅР°СЏ РјР°С‚СЂРёС†Р°
        glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        _skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

        glActiveTexture(GL_TEXTURE0);  //С‚РµРєСЃС‚СѓСЂРЅС‹Р№ СЋРЅРёС‚ 0
        glBindSampler(0, sampler);
        _cubeTex->bind();
        _skyboxShader->setIntUniform("cubeTex", 0);

        glDepthMask(GL_FALSE); //РћС‚РєР»СЋС‡Р°РµРј Р·Р°РїРёСЃСЊ РІ Р±СѓС„РµСЂ РіР»СѓР±РёРЅС‹

        _cube->draw();

        glDepthMask(GL_TRUE); //Р’РєР»СЋС‡Р°РµРј РѕР±СЂР°С‚РЅРѕ Р·Р°РїРёСЃСЊ РІ Р±СѓС„РµСЂ РіР»СѓР±РёРЅС‹


        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 0
        glBindSampler(1, _sampler_tex);
        _cubeTex2->bind();
        _shader->setIntUniform("diffuseTex", 0);


        _light.position =
                glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        _shader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix *
                                           glm::translate(glm::mat4(1.0f), lightPos));
        _shader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        // glActiveTexture(GL_TEXTURE0);
        // glBindSampler(0, sampler);
        // _texture->bind();

        // _shader->setIntUniform("textureSampler", 0);

        _shader->setMat3Uniform("normalToCameraMatrix",
                               glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _torus->modelMatrix()))));

        _shader->setMat4Uniform("modelMatrix", _torus->modelMatrix());

        _torus->draw();

        // glBindSampler(0, 0);
        // glUseProgram(0);
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
