/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

uniform bool white;
uniform bool clipY;

in vec4 color;
in float show;

out vec4 fragColor;

void main()
{
    if (show < 0.5 && clipY) {
        fragColor = vec4(0.0, 0.0, 0.0, 0.0);
        return;
    }
    if (white) {
        fragColor = vec4(1.0, 1.0, 1.0, 1.0);
    } else {
        fragColor = color;
    }
}
